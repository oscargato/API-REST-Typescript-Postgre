CREATE DATABASE node_postgre_ts;

CREATE TABLE users(
	id SERIAL PRIMARY KEY,
	name VARCHAR(200) NOT NULL,
	email TEXT NOT NULL
);

INSERT INTO users (name, email) VALUES ('Oscar Mejia','oscardongato@gmail.com');
INSERT INTO users (name, email) VALUES ('Jose Fernandez','ingoscarmejia80@gmail.com');

DESCRIBE posts