"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = express_1.default();
//import indexRoutes from './routes/index.routes';
//app.use(express.json);
//app.use(express.urlencoded({extended: false}));
//app.use(indexRoutes);
app.listen(3000);
console.log('Server on port', 3000);
